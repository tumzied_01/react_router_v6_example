import logo from "./logo.svg";
import "./App.css";
import { Link, NavLink, Routes, Route } from "react-router-dom";
import Expenses from "./component/Expenses";
import Invoices from "./component/invoices";
import Home from "./component/Home";
import Login from "./component/Login";
import NoMatchRoute from "./component/noMatch";
import InvoiceDetails from "./component/InvoiceDetails";
import { useState } from "react";
import {authenticateContext} from "./authContext";
import Logout from "./component/Logout";
import LoginRequired from "./component/LoginRequired";
function App() {
  const [isAuth,setAuth]=useState(false);
  return (
    <div className="App">
      <h1>React router example</h1>
      <nav>
        <NavLink to="/">Home</NavLink> |{" "}
        <NavLink to="/invoices">Invoices</NavLink> |{" "}
        <NavLink to="/expenses">Expenses</NavLink> |{" "}
        <NavLink to="/login">Login</NavLink>  |{" "}
        <NavLink to="/logout">logout</NavLink>
      </nav>
      <authenticateContext.Provider value={[isAuth,setAuth]}>

      
      <Routes>
        <Route path="" element={<Home />}>
          <Route path="invoices" element={ <LoginRequired> <Invoices /> </LoginRequired> }>
            <Route
              index
              element={
                <div style={{ padding: "1rem" }}>
                  <p>Select an invoice</p>
                </div>
              }
            />
            <Route path=":invoiceNumber" element={<InvoiceDetails />}></Route>
          </Route>
          <Route path="expenses" element={<LoginRequired><Expenses /></LoginRequired> }></Route>
          <Route index element={<p>this is an index path of the home page</p>}></Route>
        </Route>
        <Route path="login" element={<Login />}></Route>
        <Route path="logout" element={<Logout/>}></Route>
        {/* route that dont match anything */}
        <Route path="*" element={<NoMatchRoute></NoMatchRoute>}></Route>
      </Routes>
      </authenticateContext.Provider>
    </div>
  );
}

export default App;
