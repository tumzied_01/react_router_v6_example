import React,{useEffect,useContext} from "react";
import { authenticateContext } from "../authContext";

export default function Logout(){
    const [_,setAuth]=useContext(authenticateContext)
    useEffect(()=>{
        setAuth(false);
    },[])
    return (
        <>
            <p>You are logout</p>
        </>
    );
}