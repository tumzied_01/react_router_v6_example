import React, { useEffect,useContext } from "react";
import { Link, Outlet, useSearchParams } from "react-router-dom";

import { getInvoices } from "../data";
export default function Invoices() {



  //   react router stuff
  const [searchParams, setSearchParams] = useSearchParams({});
  let searchTerms = searchParams.get("name") || ""; // get specific search parms

  
  const onChangeHandler = (event) => {
    if (event.target.value) setSearchParams({ name: event.target.value });
    else setSearchParams({});
    
  };


  const lists = getInvoices()
    .filter((invoice) =>
      invoice.name.toLocaleLowerCase().includes(searchTerms.toLocaleLowerCase())
    )
    .map((invoice, idx) => {
      return (
        <Link to={`./${invoice.number}/`} key={idx}>
          {invoice.name}
        </Link>
      );
    });



  return (
    <>
      <h3>Invoices page</h3>

      <div style={{ float: "right" }}>
        <input
          type={"text"}
          name="search"
          onChange={onChangeHandler}
          value={searchTerms}
          aria-label="search invoices field"
          placeholder="Search by Name"
        ></input>
      </div>
      <div className="invoiceSection" style={{ clear: "both" }}>
        <div className="invoices">{lists}</div>

        <div className="invoice-details">
          <Outlet />
        </div>
      </div>
    </>
  );
}
