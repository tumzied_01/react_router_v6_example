import React, { useContext, useEffect } from "react";
import { Link, Outlet, useSearchParams,useNavigate } from "react-router-dom";
import {authenticateContext} from "../authContext";
export default function Login(){
    const [searchParams,_]=useSearchParams();
    const navigate=useNavigate();
    const [isAuth,setAuth]=useContext(authenticateContext);

    const isNext=searchParams.get('next') || null;

    const redirectTo='/';

    

    const formHandler=(e)=>{
        e.preventDefault();

        // authenticate 

        // if authenticate is not success
            //show error 
            //return
        
        // redirect to another page 

        setAuth(true);

        navigate(isNext?isNext:redirectTo);


    
    }
    
    return (
        <>
            <h2>Login</h2>

            <form onSubmit={formHandler}>

                <div>
                    <label htmlFor="id_username"> username</label>
                    <input id="id_username" type={'text'} name="username"/>

                </div>
                <div>
                    <label htmlFor="id_password"> password</label>
                    <input id="id_password" type={'password'} name="password"/>
                </div>
            
                <input id="id_" type={'submit'} name="submit" value={'submit'}/>
            </form>
        </>
    );
}