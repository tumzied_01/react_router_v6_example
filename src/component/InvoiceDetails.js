import React from "react";
import { useParams } from "react-router-dom";
import { getInvoices } from "../data";
export default function InvoiceDetails(props) {
  
  const params = useParams(); // get url params 
  
  const invoice = getInvoices().find(
    (val) => val.number == params.invoiceNumber
  );

  return (
    <>
      <div>
        <div>name: {invoice.name}</div>
        <div>number: {invoice.number}</div>
        <div>amount: {invoice.amount}</div>
        <div>due: {invoice.due}</div>
      </div>
    </>
  );
}
