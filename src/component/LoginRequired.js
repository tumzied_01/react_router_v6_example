import { useEffect, useContext } from "react";
import { authenticateContext } from "../authContext";
import { useNavigate, useLocation } from "react-router-dom";
export default function LoginRequired({ children }) {
  const [isAuth, _] = useContext(authenticateContext);
  //   react router stuff
  let current_location = useLocation();
  let navigate = useNavigate();

  useEffect(() => {
    if (!isAuth) navigate("/login?next=" + current_location.pathname);
  }, []);

  return <>{children}</>;
}
