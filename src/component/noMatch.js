import React from "react";

export default function NoMatchRoute(){
    
    return (
        <>
            <p>Nothing to see</p>
        </>
    );
}